====================================
Welcome to WL-align's documentation!
====================================

.. image:: https://gitlab.inria.fr/cobcom/wlalign/badges/master/pipeline.svg
    :target: https://gitlab.inria.fr/cobcom/wlalign/-/commits/master"
    :alt: Pipeline Status

.. image:: https://gitlab.inria.fr/cobcom/wlalign/badges/master/coverage.svg
    :target: https://gitlab.inria.fr/cobcom/wlalign/-/commits/master
    :alt: Coverage Report

.. image:: https://readthedocs.org/projects/wl-align/badge/?version=latest
    :target: https://wl-align.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status


`wlalign` is a pure Python package that implements the graph-alignment routine
based on the generalization of the Weisfeiler-Lehman algorithm proposed in
`this paper <https://doi.org/10.1162/netn_a_00199>`_.

The software provides the ``wlalign`` Python module, which includes all the
**functions and tools that are necessary for computing network alignments and
similarity**.
In particular, specific functions are devoted to:


- Computing the **graph Jaccard index** of **similarity** between two weighted graphs.
- Solving the **graph alignment problem** with **WL-align**.


The package is `available at Pypi <https://pypi.org/project/wlalign/>`_ and can
be easily installed from the command line.

.. code:: bash

    pip install wlalign


Talon is a free software released under `MIT license <LICENSE>`_.

Getting help
============
The preferred way to get assistance in running code that uses WL-align is
through the issue system of the
`Gitlab repository <https://gitlab.inria.fr/cobcom/wlalign>`_ where the
source code is available.
Developers and maintainers frequently check newly opened issues and will be
happy to help you.


Contributing guidelines
=======================
The development happens in the ``devel`` branch of the
`Gitlab repository <https://gitlab.inria.fr/cobcom/wlalign>`_, while the
``master`` is kept for the stable releases only.
We will consider only merge requests towards the ``devel`` branch.


How to cite
===========
If you publish works using WL-align, please cite us as indicated here:

    Matteo Frigo, Emilio Cruciani, David Coudert, Rachid Deriche, Emanuele
    Natale, Samuel Deslauriers-Gauthier; Network alignment and similarity
    reveal atlas-based topological differences in structural connectomes.
    Network Neuroscience 2021; doi: https://doi.org/10.1162/netn_a_00199

In section :ref:`citation` you will find the Bibtex entry.


.. toctree::
    :maxdepth: 2
    :caption: Install

    installation

.. toctree::
    :maxdepth: 2
    :caption: Command Line Interface

    command-line-interface


.. toctree::
    :maxdepth: 2
    :caption: API documentation

    api

.. toctree::
    :maxdepth: 1
    :caption: About the project

    citation
    contributors
    license
    funding



Funding
=======
The development of WL-align was funded by the European Research Council (ERC)
under the European Union’s Horizon 2020 research and innovation program (ERC
Advanced Grant agreement No 694665: `CoBCoM - Computational Brain Connectivity
Mapping <https://project.inria.fr/cobcom/>`_ ).

.. image:: img/logo_erc_eu.jpg
    :align: center
    :width: 600
